package stack;

public class Stack {
    int[] stack;
    int size;

    public Stack(int arrSize) {
        stack = new int[arrSize];
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int push(int value) {
        if (size == stack.length) {
            doubleLength();
        }
        stack[size] = value;
        size++;
        return value;
    }

    public int pop() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int result = stack[size - 1];
        stack[size - 1] = 0;
        size--;
        return result;
    }

    public int peek() {
        if (size == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return stack[size - 1];
    }

    private void doubleLength() {
        int[] newStack = new int[2 * stack.length];
        for (int i = 0; i < stack.length; i++) {
            newStack[i] = stack[i];
        }
        stack = newStack;
    }
}
