package main;

import queue.Queue;
import stack.Stack;

public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue(10);
        System.out.println(queue.isEmpty());
        System.out.println(queue.enqueue(10000));
        System.out.println(queue.enqueue(1000));
        System.out.println(queue.enqueue(5000));
        System.out.println(queue.getSize());
        System.out.println(queue.peek());
        System.out.println(queue.isEmpty());
        System.out.println(queue.dequeue() + "Deleted");


        Stack stack = new Stack(10);
        System.out.println(stack.push(10));
        System.out.println(stack.push(30));
        System.out.println(stack.push(40));
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.push(60));
        System.out.println(stack.isEmpty());
        System.out.println(stack.peek());
    }
}
